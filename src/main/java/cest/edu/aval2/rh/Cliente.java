package cest.edu.aval2.rh;

import cest.edu.aval2.rh.PessoaFisica;

public class Cliente extends PessoaFisica{
    private String email;

    public Cliente(String nome, String cpf, String email) {
        super(nome, cpf);
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
}
