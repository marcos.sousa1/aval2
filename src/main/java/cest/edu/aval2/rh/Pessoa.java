package cest.edu.aval2.rh;

import cest.edu.aval2.rh.Endereco;

public interface Pessoa {
    public String getNome ();
    public Endereco getEndereco();
    
    //public String getCpf (); esse método pode gerar conflito com a classe PessoaJuridica
}
