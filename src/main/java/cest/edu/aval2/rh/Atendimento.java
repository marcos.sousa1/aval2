package cest.edu.aval2.rh;

import java.util.Date;
import java.util.List;

import cest.edu.aval2.rh.Requisicoes;

public class Atendimento {
    private Date data;
    private Integer numero;
    private List<Requisicoes> requisisoes;

    public Atendimento(Date data, Integer numero, List<Requisicoes> requisisoes) {
        this.data = data;
        this.numero = numero;
        this.requisisoes = requisisoes;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public List<Requisicoes> getRequisisoes() {
        return requisisoes;
    }

    public void setRequisisoes(List<Requisicoes> requisisoes) {
        this.requisisoes = requisisoes;
    }
    
    
}
