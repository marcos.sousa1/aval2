package cest.edu.aval2.rh;

import cest.edu.aval2.rh.Endereco;
import cest.edu.aval2.rh.Pessoa;

public class PessoaJuridica implements Pessoa {
    private String nome;
    private String cnpj;
    private Endereco endereco;
    
    @Override
    public Endereco getEndereco() {
        return this.endereco.getEndereco();
    }
    @Override
    public String getNome() {
        return this.nome;
    }

    public PessoaJuridica(String nome, String cnpj) {
        this.nome = nome;
        this.cnpj = cnpj;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCnpj() {
        return cnpj;
    }
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    } 

}
