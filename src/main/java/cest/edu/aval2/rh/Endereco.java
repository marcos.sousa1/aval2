package cest.edu.aval2.rh;

import cest.edu.aval2.rh.Cidade;

public class Endereco {
    private Cidade cidade;
    private String logradouro;
    
    public Endereco(Cidade cidade, String logradouro) {
        this.cidade = cidade;
        this.logradouro = logradouro;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }
    
}
