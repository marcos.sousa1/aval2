package cest.edu.aval2.rh;

import cest.edu.aval2.rh.Endereco;
import cest.edu.aval2.rh.Pessoa;

public class PessoaFisica implements Pessoa {
    private String nome;
    private String cpf;
    private Endereco endereco;
    
    @Override
    public Endereco getEndereco() {
        return this.endereco.getEndereco();
    }

    @Override
    public String getNome() {
        return this.nome;
    }

    public PessoaFisica(String nome, String cpf) {
        this.nome = nome;
        this.cpf = cpf;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCpf() {
        return cpf;
    }
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
    
}
